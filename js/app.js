

$(document).ready(function(){
    
    //test
    //$("body").css("background","green");
    
    var nbparts;
    var prenom;
    
    // Afficher les ingrédients des pizzas au survol de leurs noms.
   
    $("label").hover(function(){
        $(this).find("span").toggle(0,"display");
    });
    
    
     //Afficher visuellement le nombre de parts et le nombre de pizzas approprié en dessous du champs « Nombre de parts ».
    //REMARQUE: pour changer le nombre de part de pizza avec un nombre inférieur, il faut relancer la page.
    
    $('.nb-parts input').on('input',function(){
        
        nbparts = $(this).val();
        
        if(nbparts <= 6){
            $("span.pizza-1").addClass("pizza-"+nbparts);
        } 
        else { 
            $(this).after($('<span class="pizza-pict"></span>').addClass("pizza-"+nbparts%6)); 
            if (nbparts/6 > 1){
                if(nbparts%6==0){
                    $(this).after($('<span class="pizza-pict"</span>').addClass("pizza-6")); 
                }
                for(i=0;i<nbparts/6-1;i++){
                $(this).after($('<span class="pizza-pict"></span>').addClass("pizza-6"));   
                }
                $("span.pizza-1").remove();
                if(nbparts%6==1){
                    $(this).after($('<span class="pizza-pict"></span>').addClass("pizza-1"));
                }
            } 
        }
        

    });
    

    

    //Afficher le formulaire de saisie d'adresse au clic sur le bouton "Etape suivante" puis masquer ce même bouton.
    
    $("button.btn-success").click(function(){
        $(this).fadeOut();
        $('div.infos-client').removeClass("no-display");
        
    });
    

   //Ajouter une ligne de champ d'adresse lorsque l'on clique sur le bouton "Ajouter un autre ligne d'adresse"
    /**
    $("button.btn-default").click(function(){
        $("button.btn-default").before($("div.text input")).append('<input type="text"/>');
    });
**/
    $("button.btn-default").click(function() {
        $(".add").before($('<input></input>').attr({type: 'text'}).css("display", "block"));
    })


    //Au clic sur le bouton de validation, supprimer tous les éléments de la page, et afficher un message de remerciement (Merci PRENOM ! Votre commande sera livrée dans 15 minutes).
       
     $("button.done").click(function(){
        let prenom = $("div.infos-client > div.type:first > input").val(); 
        $("div.stick-right").fadeOut();
        $('div.main').fadeOut();
        $("div.headline").find('small').html("Merci  "+ prenom +"  ! Votre commande sera livrée dans 15 minutes !");
    });




    
})





